# Output von QGIS aufteilen

## Installation
NodeJS installieren: https://nodejs.org/en/download/

## PowerShell 7 installieren
Via Microsoft Store oder via Webseite:
https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-windows?view=powershell-7


## Projekt ausführen.

Power Shell öffnen und zu einem Ordner navigieren

Git Repo clonen

```
git clone git@gitlab.com:ayalon/geojson_parser.git
cd geojson_parser
```

### Dependancies installieren

```
npm install
```

## Ausgangsfile
Ausgangsfile kommt in den Input Ordner, Dateinamen im index.js anpassen. Zudem muss das ID Mapping angepasst werden. Ich habe als ID die BFS Nummer verwendet, die ist wahrscheinlich anders bei den deutschen Gemeinden.

## Ausführen
```
npm run start
```

Dies generiert im Output die Gemeinden.
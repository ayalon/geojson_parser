const fs = require('fs')
var GeoJSON = require('geojson')

// Get content from file
var contents = fs.readFileSync('input/ch_gemeinden.geojson')
// Define to JSON type
var jsonContent = JSON.parse(contents)

var file = []
jsonContent.features.forEach(function (entry) {
  data = {
    name: entry.properties.NAME,
    id: entry.properties.BFS_NUMMER,
    geo: entry.geometry,
  }

  var template = fs.readFileSync('template.json')
  var templateJson = JSON.parse(template)
  templateJson['features'].push(entry)

  filename = 'data/' + data.id + '.json'
  console.log('Writing ' + filename)

  if (fs.existsSync(filename)) {
    var existing_file = fs.readFileSync(filename)
    var existing_file_data = JSON.parse(existing_file)
    existing_file_data['features'].push(entry)
    templateJson = existing_file_data
    console.log('Updating ' + filename)
  }

  fs.writeFileSync(filename, JSON.stringify(templateJson, null, 2), 'utf-8')
})
